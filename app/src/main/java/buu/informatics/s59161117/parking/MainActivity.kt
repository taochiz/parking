package buu.informatics.s59161117.parking

import android.content.Context
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import buu.informatics.s59161117.parking.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.IndexOutOfBoundsException

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val myParking: ArrayList<Parking> = ArrayList()
    private var slot: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        for(item in 0..2) {
            myParking.add(item, Parking("","",""))
        }
        setVisibleIsTrue()
        binding.apply{
            slot1Button?.setOnClickListener {
                slot = 1
                slot1Button.setBackgroundResource(R.drawable.btn_rounded_corner)
                slot2Button?.setBackgroundResource(R.drawable.btn_rounded_corner_empty)
                slot3Button?.setBackgroundResource(R.drawable.btn_rounded_corner_empty)

                showValue(slot)
                setVisibleIsFalse()

            }
            slot2Button?.setOnClickListener {
                slot = 2
                slot2Button.setBackgroundResource(R.drawable.btn_rounded_corner)
                slot1Button?.setBackgroundResource(R.drawable.btn_rounded_corner_empty)
                slot3Button?.setBackgroundResource(R.drawable.btn_rounded_corner_empty)

                showValue(slot)
                setVisibleIsFalse()
            }
            slot3Button?.setOnClickListener {
                slot = 3
                slot3Button.setBackgroundResource(R.drawable.btn_rounded_corner)
                slot1Button?.setBackgroundResource(R.drawable.btn_rounded_corner_empty)
                slot2Button?.setBackgroundResource(R.drawable.btn_rounded_corner_empty)

                showValue(slot)
                setVisibleIsFalse()
            }
            saveButton?.setOnClickListener {
                setValueBooking(it)
            }
            clearButton?.setOnClickListener {
                setVisibleIsTrue()
            }
        }

    }

    private fun setVisibleIsTrue() {
        binding.apply {
            licenseplate_edit.visibility = View.GONE
            brand_edit.visibility = View.GONE
            name_edit.visibility = View.GONE
            save_button.visibility = View.GONE
            clear_button.visibility = View.GONE
        }
        if (slot == 1) {
            binding.slot1Button?.setText("ว่าง")
            binding.slot1Button?.setBackgroundResource(R.drawable.btn_rounded_corner_empty)
            myParking.removeAt(0)
            myParking.add(0, Parking("","",""));
            Toast.makeText(this, "ล้างข้อมูลเรียบร้อย", Toast.LENGTH_LONG).show()
        } else if (slot == 2) {
            binding.slot2Button?.setText("ว่าง")
            binding.slot2Button?.setBackgroundResource(R.drawable.btn_rounded_corner_empty)
            myParking.removeAt(1)
            myParking.add(1,Parking("","",""));
            Toast.makeText(this, "ล้างข้อมูลเรียบร้อย", Toast.LENGTH_LONG).show()
        } else if (slot == 3) {
            binding.slot3Button?.setText("ว่าง")
            binding.slot3Button?.setBackgroundResource(R.drawable.btn_rounded_corner_empty)
            myParking.removeAt(2)
            myParking.add(2,Parking("","",""));
            Toast.makeText(this, "ล้างข้อมูลเรียบร้อย", Toast.LENGTH_LONG).show()
        }
    }

    private fun setVisibleIsFalse() {
        binding.apply {
            licenseplate_edit.visibility = View.VISIBLE
            brand_edit.visibility = View.VISIBLE
            name_edit.visibility = View.VISIBLE
            save_button.visibility = View.VISIBLE
            clear_button.visibility = View.VISIBLE
        }
    }

    private fun setValueBooking(view: View) {
        val licenseplateEdit = binding.licenseplateEdit?.text.toString()
        val brand = binding.brandEdit?.text.toString()
        val name = binding.nameEdit?.text.toString()
        if(licenseplateEdit == "" || brand == "" || name == ""){
            Toast.makeText(this, "กรุณากรอกข้อมูล", Toast.LENGTH_LONG).show()
        }else {
            myParking.add(slot - 1, Parking(licenseplateEdit, brand, name))
            val setTextSlot = myParking.get(slot - 1).licenseplant
            if (slot == 1) {
                binding.slot1Button?.setText(setTextSlot)
                Toast.makeText(this, "บันทึกข้อมูลเรียบร้อย", Toast.LENGTH_LONG).show()
            } else if (slot == 2) {
                binding.slot2Button?.setText(setTextSlot)
                Toast.makeText(this, "บันทึกข้อมูลเรียบร้อย", Toast.LENGTH_LONG).show()
            } else if (slot == 3) {
                binding.slot3Button?.setText(setTextSlot)
                Toast.makeText(this, "บันทึกข้อมูลเรียบร้อย", Toast.LENGTH_LONG).show()
            }

            binding.apply {
                licenseplate_edit.visibility = View.GONE
                brand_edit.visibility = View.GONE
                name_edit.visibility = View.GONE
                save_button.visibility = View.GONE
                clear_button.visibility = View.GONE
            }
            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }


    private fun showValue(slot: Int) {
        binding.apply {
            licenseplate_edit.setText(myParking.get(slot-1).licenseplant)
            brand_edit.setText(myParking.get(slot-1).brand)
            name_edit.setText(myParking.get(slot-1).name)
        }
    }


}
